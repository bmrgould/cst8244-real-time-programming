/*
 ** fork.c -- demonstrates usage of fork() and wait()
 **
 ** Read:	http://beej.us/guide/bgipc/html/single/bgipc.html#fork
 ** Source:	http://beej.us/guide/bgipc/examples/fork1.c
 **
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void sigint_handler(int sig);

volatile sig_atomic_t usr1Happened = 0;

int main(void) {
	char c[140];
	pid_t pid;
	int numChildren;
	int i = 0;

	struct sigaction sa;

		sa.sa_handler = sigint_handler;
		sa.sa_flags = 0; // or SA_RESTART
		sigemptyset(&sa.sa_mask);

		if (sigaction(SIGUSR1, &sa, NULL) == -1) {
			printf("error");
			perror("sigaction");
			exit(1);
		}

	printf("Enter the number of children: \n");
	if (fgets(c, sizeof c, stdin) == NULL)
			perror("fgets");
	sscanf(c, "%d", &numChildren);

	int rv;

	printf("PID = %d: Parent running... \n", getpid());
	for (i = 0; i < numChildren; i++ ){
		switch(pid = fork()){
			case -1:

				perror("fork"); /* something went wrong */

				exit(1); /* parent exits */
			case 0:
				printf("PID = %d: Child running... \n", getpid());
				while(usr1Happened != 1){
						// wait
					}

				printf("PID = %d : Child Received USR1. \n", getpid());

				printf("PID = %d: Child Exiting. \n", getpid());
				exit(EXIT_SUCCESS);

			default:
				printf("");
		}
	}
	for( i = 0 ; i < numChildren ; i++){
		wait(&rv);
	}


	printf("PID = %d: Children finished, parent exiting. \n", getpid());


	return 0;
}

void sigint_handler(int sig) {
	usr1Happened = 1;
}
