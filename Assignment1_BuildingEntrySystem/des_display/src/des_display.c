#include <stdio.h>
#include <stdlib.h>
#include <sys/neutrino.h>
#include <sys/netmgr.h>
#include <errno.h>
#include <unistd.h>
#include <process.h>
#include <string.h>
#include "../../des_controller/src/des.h"

int main(int argc, char *argv[]) {
	int rcvid;        // indicates who we should reply to
	int chid;         // the channel ID
	Person person;

	// create a channel for the controller --- Phase I
	chid = ChannelCreate(0);
	if (chid == -1)
	{
		perror("failed to create the channel.");
		exit(EXIT_FAILURE);
	}
	printf("Display PID is %d\tAnd the Channel ID (chid) is: %d\n", getpid(), chid);

	// this is typical of a server:  it runs forever --- Phase II
	while (1)
	{
		// TOTHINK: &display, make display object instead of having it in Person?
		rcvid = MsgReceive(chid, &person, sizeof(person), NULL);
		if (person.state == SCAN_STATE){
			printf("%s %d \n", outMessage[person.state], person.id);
		}
		else if (person.state == WEIGHT_STATE){
			printf("%s %d \n", outMessage[person.state], person.weight);
		}
		else {
			printf("%s\n",outMessage[person.state]);
		}
		if (person.state== EXIT_STATE){
			MsgReply(rcvid, EOK, NULL,0);
			break;
		}

		MsgReply(rcvid, EOK, &person, sizeof(person));
	}

	// destroy the channel when done --- Phase III
	ChannelDestroy(chid);
	printf("Display Exit. \n");
	return EXIT_SUCCESS;
}
