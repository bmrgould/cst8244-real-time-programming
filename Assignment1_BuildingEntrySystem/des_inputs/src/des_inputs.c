#include <stdio.h>
#include <stdlib.h>
#include <sys/neutrino.h>
#include <sys/netmgr.h>
#include <errno.h>
#include <unistd.h>
#include <process.h>
#include <string.h>
#include "../../des_controller/src/des.h"

int main(int argc, char *argv[]) {
	int  coid;
	pid_t pid;
	Person person;

	if (argc != 2){
		perror("Wrong number of arguments.");
		exit(EXIT_FAILURE);
	}
	pid = atoi(argv[1]);

	// establish a connection --- Phase I
	coid = ConnectAttach (ND_LOCAL_NODE, pid, 1, _NTO_SIDE_CHANNEL, 0);
	if (coid == -1) {
		fprintf (stderr, "Couldn't ConnectAttach\n");
		perror (NULL);
		exit (EXIT_FAILURE);
	}
	printf("Client PID is %d\n", getpid());

	// send the message --- Phase II

	while(1){
		printf("Enter the event type (ls= left scan, rs= right scan, ws= weight scale, lo =left open, ro=right open, lc = left closed, rc = right closed , gru = guard right unlock, grl = guard right lock, gll=guard left lock, glu = guard left unlock): \n");
		scanf("%s", &person.current);

		if ((strcmp(person.current, inMessage[LEFT_SCAN_INPUT]) == 0) || (strcmp(person.current, inMessage[RIGHT_SCAN_INPUT]) == 0) ){
			printf("%s", "Enter the person_id: \n");
			scanf("%d", &person.id);
		}

		else if (strcmp(person.current, inMessage[WEIGHT_INPUT]) == 0){
			printf("%s", "Enter the weight: \n");
			scanf("%d", &person.weight);
		}
		if (strcmp(person.current, inMessage[EXIT_INPUT]) == 0){
			if (MsgSend (coid, &person, sizeof(person), NULL,0) == -1L) {
				fprintf (stderr, "Error during MsgSend\n");
				perror (NULL);
				exit (EXIT_FAILURE);
			}
			break;
		}
		if (MsgSend (coid, &person, sizeof(person), &person, sizeof(person)) == -1L) {
			fprintf (stderr, "Error during MsgSend\n");
			perror (NULL);
			exit (EXIT_FAILURE);
		}


	} // end while(1)

	//Disconnect from the channel --- Phase III
	ConnectDetach(coid);
	printf("Input Exit. \n");
	return EXIT_SUCCESS;
}
