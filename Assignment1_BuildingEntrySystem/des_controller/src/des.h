/*
 * des.h
 *
 */

#ifndef DES_H
#define DES_H

#define NUM_STATES 12
typedef enum {
	START_STATE = 0,
	SCAN_STATE = 1,
	LEFT_UNLOCK_STATE = 2,
	RIGHT_UNLOCK_STATE = 3,
	LEFT_OPEN_STATE = 4,
	RIGHT_OPEN_STATE = 5,
	WEIGHT_STATE = 6,
	LEFT_CLOSE_STATE = 7,
	RIGHT_CLOSE_STATE = 8,
	LEFT_LOCK_STATE = 9,
	RIGHT_LOCK_STATE = 10,
	EXIT_STATE = 11
} State;

#define NUM_INPUTS 12
typedef enum {
	LEFT_SCAN_INPUT = 0,
	RIGHT_SCAN_INPUT = 1,
	LEFT_UNLOCK_INPUT = 2,
	RIGHT_UNLOCK_INPUT = 3,
	LEFT_OPEN_INPUT = 4,
	RIGHT_OPEN_INPUT = 5,
	WEIGHT_INPUT = 6,
	LEFT_CLOSE_INPUT = 7,
	RIGHT_CLOSE_INPUT = 8,
	LEFT_LOCK_INPUT = 9,
	RIGHT_LOCK_INPUT = 10,
	EXIT_INPUT = 11
} Input;


typedef enum {
	LEFT = 0,
	RIGHT = 1
} Direction;

typedef enum {
	START_MSG = 0,
	SCAN_MSG = 1,
	LEFT_UNLOCK_MSG = 2,
	RIGHT_UNLOCK_MSG = 3,
	LEFT_OPEN_MSG = 4,
	RIGHT_OPEN_MSG = 5,
	WEIGHT_MSG = 6,
	LEFT_LOCK_MSG = 7,
	RIGHT_LOCK_MSG = 8,
	EXIT_MSG = 9,
	STOP_MSG = 10
} Output;

#define NUM_OUTPUTS 12
const char *outMessage[NUM_OUTPUTS] = {
		"Door system started",
		"Person has been scanned and their ID is ",
		"Left door unlocked ",
		"Right door unlocked ",
		"Left door open ",
		"Right door open ",
		"Person has been weighed and their weight is ",
		"Left door closed ",
		"Right door closed ",
		"Left door locked ",
		"Right door locked ",
		"Exiting. "
};

const char *inMessage[NUM_INPUTS] = {
		"ls",
		"rs",
		"glu",
		"gru",
		"lo",
		"ro",
		"ws",
		"lc",
		"rc",
		"gll",
		"grl",
		"exit"
};

struct person {
	int id;
	int weight;
	int state;
	int direction;
	char current[128];
} typedef Person;


#endif
