#include <stdio.h>
#include <stdlib.h>
#include <sys/neutrino.h>
#include <sys/netmgr.h>
#include <errno.h>
#include <unistd.h>
#include <process.h>
#include <string.h>
#include "des.h"

Person person;
int coid;

typedef void *(*StateFunc)();
StateFunc statefunc;
char rmsg [200];

void *start_state();
void *scan_state();
void *left_unlock();
void *right_unlock();
void *left_lock();
void *right_lock();
void *left_open();
void *right_open();
void *left_close();
void *right_close();
void *weight_func();
void exit_state();

int main(int argc, char *argv[]) {
	pid_t pid;
	int chid;
	int rcvid;
	statefunc = start_state;

	if (argc < 2){
		perror("Wrong number of arguments.");
		exit(EXIT_FAILURE);
	}
	pid = atoi(argv[1]);

	chid = ChannelCreate(0);
	if (chid == -1)
	{
		perror("failed to create the channel.");
		exit(EXIT_FAILURE);
	}
	printf("Controller PID is %d\tAnd the Channel ID (chid) is: %d\n", getpid(), chid);

	coid = ConnectAttach (ND_LOCAL_NODE, pid, 1, _NTO_SIDE_CHANNEL, 0);
	if (coid == -1) {
		fprintf (stderr, "Couldn't ConnectAttach\n");
		perror (NULL);
		exit (EXIT_FAILURE);
	}

	while(1){
		rcvid = MsgReceive(chid, &person, sizeof(person), NULL);
		if (strcmp(person.current, inMessage[EXIT_INPUT]) == 0){
			exit_state(rcvid);
			break;
		}

		statefunc = (StateFunc)(*statefunc)();
		sleep(1);
		MsgReply(rcvid, EOK, &person, sizeof(person));

	}

	ChannelDestroy(chid);
	ConnectDetach(coid);
	printf("Controller Exit. \n");
	return EXIT_SUCCESS;
}

void *start_state() {
	if (strcmp(person.current, inMessage[LEFT_SCAN_INPUT]) == 0){
		person.direction = RIGHT;
		person.state = SCAN_STATE;
		if (MsgSend(coid, &person, sizeof(person), &person, sizeof(person)) == -1L ){
			perror ("Controller - Message Send Error - Start_State");
			exit (EXIT_FAILURE);
		}
		return scan_state;
	}
	if (strcmp(person.current, inMessage[RIGHT_SCAN_INPUT]) == 0){
		person.direction = LEFT;
		person.state = SCAN_STATE;
		if (MsgSend(coid, &person, sizeof(person), &person, sizeof(person)) == -1L ){
			perror ("Controller - Message Send Error - Start_State");
			exit (EXIT_FAILURE);
		}
		return scan_state;
	}

	return start_state;
}

void *scan_state(){
	if(person.direction == RIGHT){
		if (strcmp(person.current, inMessage[LEFT_UNLOCK_INPUT]) == 0){
			person.state = LEFT_UNLOCK_STATE;
			if (MsgSend(coid, &person, sizeof(person), &person, sizeof(person)) == -1L ){
				perror ("Controller - Message Send Error");
				exit (EXIT_FAILURE);
			}
			return left_unlock;
		}
	}
	else if (person.direction == LEFT){
		if (strcmp(person.current, inMessage[RIGHT_UNLOCK_INPUT]) == 0){
			person.state = RIGHT_UNLOCK_STATE;
			if (MsgSend(coid, &person, sizeof(person), &person, sizeof(person)) == -1L ){
				perror ("Controller - Message Send Error");
				exit (EXIT_FAILURE);
			}
			return right_unlock;
		}
	}
	return scan_state;
}

void *left_unlock() {
	if(strcmp(person.current, inMessage[LEFT_OPEN_INPUT]) == 0){
		person.state = LEFT_OPEN_STATE;
		if (MsgSend(coid, &person, sizeof(person), &person, sizeof(person)) == -1L ){
			perror ("Controller - Message Send Error");
			exit (EXIT_FAILURE);
		}
		return left_open;
	}
	return left_unlock;
}

void *right_unlock() {
	if(strcmp(person.current, inMessage[RIGHT_OPEN_INPUT]) == 0){
		person.state = RIGHT_OPEN_STATE;
		if (MsgSend(coid, &person, sizeof(person), &person, sizeof(person)) == -1L ){
			perror ("Controller - Message Send Error");
			exit (EXIT_FAILURE);
		}
		return right_open;
	}
	return right_unlock;
}

void *left_open() {
	if(person.direction == RIGHT){
		if(strcmp(person.current, inMessage[WEIGHT_INPUT]) == 0){
			person.state = WEIGHT_STATE;
			if (MsgSend(coid, &person, sizeof(person), &person, sizeof(person)) == -1L ){
				perror ("Controller - Message Send Error");
				exit (EXIT_FAILURE);
			}
			return weight_func;
		}
	}
	else if(person.direction == LEFT){
		if(strcmp(person.current, inMessage[LEFT_CLOSE_INPUT]) == 0){
			person.state = LEFT_CLOSE_STATE;
			if (MsgSend(coid, &person, sizeof(person), &person, sizeof(person)) == -1L ){
				perror ("Controller - Message Send Error");
				exit (EXIT_FAILURE);
			}
			return left_close;
		}
	}
	return left_open;
}

void *right_open() {
	if(person.direction == RIGHT) {
		if(strcmp(person.current, inMessage[RIGHT_CLOSE_INPUT]) == 0){
			person.state = RIGHT_CLOSE_STATE;
			if (MsgSend(coid, &person, sizeof(person), &person, sizeof(person)) == -1L ){
				perror ("Controller - Message Send Error");
				exit (EXIT_FAILURE);
			}
			return right_close;
		}
	}else if(person.direction == LEFT){
		if(strcmp(person.current, inMessage[WEIGHT_INPUT]) == 0){
			person.state = WEIGHT_STATE;
			if (MsgSend(coid, &person, sizeof(person), &person, sizeof(person)) == -1L ){
				perror ("Controller - Message Send Error");
				exit (EXIT_FAILURE);
			}
			return weight_func;
		}
	}
	return right_open;
}

void *weight_func() {
	if(person.direction == RIGHT){
		if(strcmp(person.current, inMessage[LEFT_CLOSE_INPUT]) == 0){
			person.state = LEFT_CLOSE_STATE;
			if (MsgSend(coid, &person, sizeof(person), &person, sizeof(person)) == -1L ){
				perror ("Controller - Message Send Error");
				exit (EXIT_FAILURE);
			}
			return left_close;
		}

	}
	if(person.direction == LEFT){
		if(strcmp(person.current, inMessage[RIGHT_CLOSE_INPUT]) == 0){
			person.state = RIGHT_CLOSE_STATE;
			if (MsgSend(coid, &person, sizeof(person), &person, sizeof(person)) == -1L ){
				perror ("Controller - Message Send Error");
				exit (EXIT_FAILURE);
			}
			return right_close;
		}

	}
	return weight_func;
}

void *left_close() {
	if(strcmp(person.current, inMessage[LEFT_LOCK_INPUT]) == 0){
		person.state = LEFT_LOCK_STATE;
		if (MsgSend(coid, &person, sizeof(person), &person, sizeof(person)) == -1L ){
			perror ("Controller - Message Send Error");
			exit (EXIT_FAILURE);
		}
		return left_lock;
	}
	return left_close;
}

void *right_close() {
	if(strcmp(person.current, inMessage[RIGHT_LOCK_INPUT]) == 0){
		person.state = RIGHT_LOCK_STATE;
		if (MsgSend(coid, &person, sizeof(person), &person, sizeof(person)) == -1L ){
			perror ("Controller - Message Send Error");
			exit (EXIT_FAILURE);
		}
		return right_lock;
	}
	return right_close;
}

void *left_lock() {
	if(person.direction == RIGHT) {
		if(strcmp(person.current, inMessage[RIGHT_UNLOCK_INPUT]) == 0){
			person.state = RIGHT_UNLOCK_STATE;
			if (MsgSend(coid, &person, sizeof(person), &person, sizeof(person)) == -1L ){
				perror ("Controller - Message Send Error");
				exit (EXIT_FAILURE);
			}
			return right_unlock;
		}
	}
	else if(person.direction == LEFT){
		if(strcmp(person.current, inMessage[EXIT_INPUT]) == 0){
			person.state = EXIT_STATE;
			if (MsgSend(coid, &person, sizeof(person), &person, sizeof(person)) == -1L ){
				perror ("Controller - Message Send Error");
				exit (EXIT_FAILURE);
			}
			return exit_state;
		}
	}
	return left_lock;
}

void *right_lock() {
	if(person.direction == RIGHT) {
		if(strcmp(person.current, inMessage[EXIT_INPUT]) == 0){
			person.state = EXIT_STATE;
			if (MsgSend(coid, &person, sizeof(person), &person, sizeof(person)) == -1L ){
				perror ("Controller - Message Send Error");
				exit (EXIT_FAILURE);
			}
			return exit_state;
		}
	}
	else if(person.direction == LEFT){
		if(strcmp(person.current, inMessage[LEFT_UNLOCK_INPUT]) == 0){
			person.state = LEFT_UNLOCK_STATE;
			if (MsgSend(coid, &person, sizeof(person), &person, sizeof(person)) == -1L ){
				perror ("Controller - Message Send Error");
				exit (EXIT_FAILURE);
			}
			return left_unlock;
		}
	}
	return right_lock;
}

void exit_state(int rcvid) {
	person.state = EXIT_STATE;
	if (MsgSend(coid, &person, sizeof(person), NULL, 0) == -1L ){
		perror ("Controller - Message Send Error - Exit_State");
		exit (EXIT_FAILURE);
	}
	MsgReply(rcvid, EOK, NULL,0);
}
