Title { 
	Lab 7 - Yet Another Simple Resource Manager 
}

Authour { 
	@author Brandon Gould
}

Status {
	Completed. Screenshot 1 is the required scenario. Screenshot 2 shows the too large functionality as displayed in the reference screenshot. 
}

Known Issues {
	Nothing.
}

Expected Grade {
	A+ - meets full marking scheme requirements.
}