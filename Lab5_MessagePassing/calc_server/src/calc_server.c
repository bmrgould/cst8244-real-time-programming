#include <stdio.h>
#include <stdlib.h>
#include <process.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/neutrino.h>
#include "calc_message.h"

int main(int argc, char *argv[]) {
	int rcvid;        // indicates who we should reply to
	int chid;         // the channel ID
	client_send_t message;
	server_response_t reply;

	// create a channel --- Phase I
	chid = ChannelCreate(0);
	if (chid == -1)
	{
		perror("failed to create the channel.");
		exit(EXIT_FAILURE);
	}
	printf("Calculator Server PID is %d \n", getpid(), chid);

	// Phase II
	    while (1)
	    {
	        rcvid = MsgReceive(chid, &message, sizeof(message),
	                           NULL);

	        //Calculator
	        reply.statusCode = SRVR_OK; //start ok
	        switch(message.operator) {
	        case '+' :
	        	reply.answer = (message.left_hand + message.right_hand);
	        	break;
	        case '-' :
	        	reply.answer = (message.left_hand - message.right_hand);
	        	break;
	        case 'x' :
	        case 'X' :
	        	reply.answer = (message.left_hand * message.right_hand);
	        	break;
	        case '/':
	        	//check for division by 0
	        	if (message.right_hand == 0){
	        		reply.statusCode = SRVR_UNDEFINED;
	        		sprintf(reply.errorMsg, "UNDEFINED.");
	        		break;
	        	}
	        	reply.answer = (message.left_hand / message.right_hand);
	        	break;
	        default:
	        	reply.statusCode = SRVR_INVALID_OPERATOR;
	        	sprintf(reply.errorMsg, "INVALID OPERATOR.");
	        }

	        MsgReply(rcvid, EOK, &reply, sizeof(reply));
	    }

	    // destroy the channel when done --- Phase III
	    ChannelDestroy(chid);


	return EXIT_SUCCESS;
}
