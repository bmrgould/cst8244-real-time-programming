#include <stdio.h>
#include <stdlib.h>
#include <process.h>
#include <sys/neutrino.h>
#include <sys/netmgr.h>
#include <string.h>
#include "calc_message.h"

/**
 * Msg_Passing_Client.c
 *
 * Usage: ./Msg_Passing_Client PID_of_Server
 *
 * Edit the Launch Configuration for the client (gear icon) > Arguments tab > enter PID of server
 */
int main (int argc, char* argv[])
{
	//client0, PID1, Left2, Op3, Right4
	client_send_t message;
	server_response_t reply;
	int  coid;
	pid_t serverpid;
	int result;
	char *operator;

	//Validate number of command-line arguments.
	if (argc == 1) {
		perror("Usage: ./calc_client <Calc-Server-PID> left_operand operator right_operand\n");
		exit (EXIT_SUCCESS);
	} else if (argc != 5){
		perror("Wrong number of command-line arguments.\n");
		exit (EXIT_FAILURE);
	}
	serverpid = atoi(argv[1]);
	message.left_hand = atoi(argv[2]);
	message.right_hand = atoi(argv[4]);

	operator = argv[3];
	if (*operator == '+' || *operator == '-' || *operator == 'x' || *operator == '/'){
		message.operator = *operator;
	}

	// establish a connection --- Phase I
	coid = ConnectAttach (ND_LOCAL_NODE, serverpid, 1, _NTO_SIDE_CHANNEL, 0);
	if (coid == -1) {
		fprintf (stderr, "Couldn't Connect Attach\n");
		perror (NULL);
		exit (EXIT_FAILURE);
	}

	// send the message --- Phase II
    result = MsgSend(coid, &message, sizeof (message), &reply, sizeof(reply));
    if (result == -1) {
		fprintf (stderr, "Error during MsgSend\n");
		perror (NULL);
		exit (EXIT_FAILURE);
	}

    switch(reply.statusCode){
    case SRVR_OK:
    	printf ("The server has calculated the result of %d %c %d as %f \n", message.left_hand, message.operator, message.right_hand, reply.answer);
    	break;
    case SRVR_UNDEFINED:
    case SRVR_INVALID_OPERATOR:
    case SRVR_OVERFLOW:
    	printf("Error message from server: %s \n", reply.errorMsg);
    	break;
    default:
    	printf("Unknown Status.\n");
    }


	//Disconnect from the channel --- Phase III
	ConnectDetach(coid);
	return EXIT_SUCCESS;
}
